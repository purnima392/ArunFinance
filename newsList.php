<?php include("header.php")?>
  
  <!-- MAIN CONTENT -->
  <div class="pg-opt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Notices & News</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        
                        <li class="active">Notices & News</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
  <section class="slice white inset-shadow-1 bb">
    <div class="wp-section">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <?php include('sidebar.php')?>
          </div>
          <div class="col-md-9">
           <div class="wp-masonry-wrapper wp-masonry-2-cols">
              <div class="wp-masonry-gutter"></div>
              
              <!-- Masonry block with standard image example -->
              <div class="wp-masonry-block">
                <div class="post-item style1">
                  <div class="post-content-wr">
                    <div class="post-meta-top">
                      <div class="post-image"> <a href="newsDetail.php"> <img src="images/finance.jpg" alt=""> </a> </div>
                      <h2 class="post-title"> <a href="newsDetail.php">The true sign of intelligence is not knowledge but imagination</a> </h2>
                    </div>
                    <div class="post-content clearfix">
                      <div class="post-tags">Posted in <a href="#">HOTELS</a>, <a href="#">SPECIAL PROMOS</a>, <a href="#">SUMMER</a></div>
                      <div class="post-comments"><strong>23</strong>comments</div>
                      <div class="post-desc">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                      </div>
                    </div>
                    <div class="post-meta-bot clearfix"> <span class="post-author">WRITTEN BY <a href="#">MARC JACOBS</a></span> <a href="#" class="like-button checked pull-right" title="Like this post"> <span class="button"> <i class="fa fa-heart"></i> </span> <span class="count">33</span> </a> </div>
                  </div>
                </div>
              </div>
              
              <!-- Masonry block with standard image example -->
              <div class="wp-masonry-block">
                <div class="post-item style1">
                  <div class="post-content-wr">
                    <div class="post-meta-top">
                      <div class="post-image"> <a href="newsDetail.php"> <img src="images/bg2.jpg" alt=""> </a> </div>
                      <h2 class="post-title"> <a href="newsDetail.php">The true sign of intelligence is not knowledge but imagination</a> </h2>
                    </div>
                    <div class="post-content clearfix">
                      <div class="post-tags">Posted in <a href="#">HOTELS</a>, <a href="#">SPECIAL PROMOS</a>, <a href="#">SUMMER</a></div>
                      <div class="post-comments"><strong>23</strong>comments</div>
                      <div class="post-desc">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                      </div>
                    </div>
                    <div class="post-meta-bot clearfix"> <span class="post-author">WRITTEN BY <a href="#">MARC JACOBS</a></span> <a href="#" class="like-button checked pull-right" title="Like this post"> <span class="button"> <i class="fa fa-heart"></i> </span> <span class="count">33</span> </a> </div>
                  </div>
                </div>
              </div>
              
             
              
              <!-- Masonry block with standard image example -->
              <div class="wp-masonry-block">
                <div class="post-item style1">
                  <div class="post-content-wr">
                    <div class="post-meta-top">
                      <div class="post-image"> <a href="newsDetail.php"> <img src="images/TH19Homeloan.jpg" alt=""> </a> </div>
                      <h2 class="post-title"> <a href="newsDetail.php">The true sign of intelligence is not knowledge but imagination</a> </h2>
                    </div>
                    <div class="post-content clearfix">
                      <div class="post-tags">Posted in <a href="#">HOTELS</a>, <a href="#">SPECIAL PROMOS</a>, <a href="#">SUMMER</a></div>
                      <div class="post-comments"><strong>23</strong>comments</div>
                      <div class="post-desc">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                      </div>
                    </div>
                    <div class="post-meta-bot clearfix"> <span class="post-author">WRITTEN BY <a href="#">MARC JACOBS</a></span> <a href="#" class="like-button checked pull-right" title="Like this post"> <span class="button"> <i class="fa fa-heart"></i> </span> <span class="count">33</span> </a> </div>
                  </div>
                </div>
              </div>
              
             
             
              
            </div>            
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("footer.php")?>