<?php include("header.php")?>
  
  <!-- MAIN CONTENT -->
  <div class="pg-opt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Saving Scheme</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        
                        <li class="active">Saving Scheme</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
  <section class="slice white inset-shadow-1 bb">
    <div class="wp-section">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <aside class="sidebar">
              <div class="block">
                <div class="section-title-wr">
                  <h3 class="section-title left"><i class="fa fa-calendar-o"></i><span>News & Events</span></h3>
                </div>
                <ul class="list">
                  <li><a href="#">सोह्रौं वार्षिक साधारण सभा</a>
                    <p>पोखरा,  ४ मंसिर २०७३| जय मनकामना बचत तथा ऋण सहकारी संस्था लि.को संस्थाका अध्यक्ष नारायण पौडेलको अध्यक्षतामा पोखरामा शनिबार सम्पन्न भयो ।</p>
                    <a href="#" class="read-btn">Read More</a> </li>
                  <li><a href="#">सोह्रौं वार्षिक साधारण सभा</a>
                    <p>पोखरा,  ४ मंसिर २०७३| जय मनकामना बचत तथा ऋण सहकारी संस्था लि.को संस्थाका अध्यक्ष नारायण पौडेलको अध्यक्षतामा पोखरामा शनिबार सम्पन्न भयो ।</p>
                    <a href="#" class="read-btn">Read More</a> </li>
                </ul>
              </div>
              <div class="block">
                <div class="section-title-wr">
                  <h3 class="section-title left"><i class="fa fa-bank"></i><span>Deposit Scheme</span></h3>
                </div>
                <ul class="list btn-list">
                  <li><a href="#"> नारी आवधिक <span>बचत </span></a> </li>
                  <li><a href="#">बाल शैक्षिक<span>बचत </span></a> </li>
                  <li><a href="#">मासिक <span>बचत </span></a> </li>
                  <li><a href="#">मुद्दती <span>खाता </span></a> </li>
                </ul>
              </div>
              <div class="block">
                <div class="section-title-wr">
                  <h3 class="section-title left"><i class="fa fa-facebook"></i><span>Follow Us Facebook</span></h3>
                </div>
                
                
              </div>
            </aside>
          </div>
          <div class="col-md-9">
            <div class="section-title-wr">
                    <h3 class="section-title left">
                        <span>संस्थाले प्रदान गर्ने सेवाहरुः</span>
                        
                    </h3>
                </div>
                <div class="row mt-20">
                    <div class="col-md-6 col-sm-6 ">
                        <div class="icon-block icon-block-1">
                            <div class="icon-block-item">
                                <img src="images/icon1.png" alt="Deposits and withdrawals">
                            </div>
                            <div class="icon-block-body">
                                <h4 class="">सदस्य÷क्रमिक बचत</h4>
                                <ul class="service">
                                	<li><strong>विशेषताः</strong> सदस्यले क्रमिक रुपमा नियमित बचत गर्नुपर्ने ।</li>
                                    <li><strong>न्यूनतम मौज्दातः</strong> रु. १०० ।–</li>
                                    <li><strong>ब्याजदरः</strong> ७.५०%</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 ">
                        <div class="icon-block icon-block-1">
                            <div class="icon-block-item">
                                 <img src="images/icon2.png" alt="Purchase and sale of foreign currency">
                            </div>
                            <div class="icon-block-body">
                                <h4 class="">नारी बचत</h4>
                                <ul class="service">
                                	<li><strong>विशेषताः</strong> महिला तथा गृहिणीहरुले बचत गर्न सक्ने ।</li>
                                    <li><strong>न्यूनतम मौज्दातः</strong> रु. १०० ।–</li>
                                    <li><strong>ब्याजदरः</strong> ७.७५%</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 ">
                        <div class="icon-block icon-block-1">
                            <div class="icon-block-item">
                                <img src="images/icon6.png" alt="">
                            </div>
                            <div class="icon-block-body">
                                <h4 class="">जेष्ठ नागरिक बचत</h4>
                                <ul class="service">
                                	<li><strong>विशेषताः</strong> ६० वर्ष नाघेका वृद्धवृद्धाहरुले बचत गर्न सक्ने ।</li>
                                    <li><strong>न्यूनतम मौज्दातः</strong> रु. १०० ।–</li>
                                    <li><strong>ब्याजदरः</strong> ८.००%</li>
                                </ul>
                            </div>
                        </div>
                    </div>    
                    <div class="col-md-6 col-sm-6 ">
                        <div class="icon-block icon-block-1">
                            <div class="icon-block-item">
                                <img src="images/icon3.png" alt="">
                            </div>
                            <div class="icon-block-body">
                                <h4 class="">बाल बचत</h4>
                                <ul class="service">
                                	<li><strong>विशेषताः</strong> १२ वर्ष भन्दा मुनिका नाबालकले बचत गर्न सक्ने ।</li>
                                    <li><strong>न्यूनतम मौज्दातः</strong> रु. १०० ।–</li>
                                    <li><strong>ब्याजदरः</strong>८.२५%</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 ">
                        <div class="icon-block icon-block-1">
                            <div class="icon-block-item">
                                <img src="images/icon4.png" alt="">
                            </div>
                            <div class="icon-block-body">
                                <h4 class="">जय नमुना बचत</h4>
                                <ul class="service">
                                	<li><strong>विशेषताः</strong> न्युनतम मौज्दात कम्तिमा १ वर्ष रहनु पर्ने ।</li>
                                    <li><strong>न्यूनतम मौज्दातः</strong> रु. १०,००,००० ।–</li>
                                    <li><strong>ब्याजदरः</strong>१०.००%</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 ">
                        <div class="icon-block icon-block-1">
                            <div class="icon-block-item">
                                <img src="images/icon4.png" alt="">
                            </div>
                            <div class="icon-block-body">
                                <h4 class="">खुत्रुके बचत</h4>
                                <ul class="service">
                                	<li><strong>विशेषताः</strong> जुनसुकै बेला जति पनि रकम राख्न र झिक्न सकिने ।</li>
                                    <li><strong>न्यूनतम मौज्दातः</strong> रु. १०० ।–</li>
                                    <li><strong>ब्याजदरः</strong>७.००%</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 ">
                        <div class="icon-block icon-block-1">
                            <div class="icon-block-item">
                                <img src="images/icon4.png" alt="">
                            </div>
                            <div class="icon-block-body">
                                <h4 class="">साधारण बचत</h4>
                                <ul class="service">
                                	<li><strong>विशेषताः</strong> जुनसुकै बेला जति पनि रकम राख्न र झिक्न सकिने ।</li>
                                    <li><strong>न्यूनतम मौज्दातः</strong> रु. १,००० ।–</li>
                                    <li><strong>ब्याजदरः</strong>७.००%</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 ">
                        <div class="icon-block icon-block-1">
                            <div class="icon-block-item">
                                <img src="images/icon4.png" alt="">
                            </div>
                            <div class="icon-block-body">
                                <h4 class="">कर्जा बचत</h4>
                                <ul class="service">
                                	<li><strong>विशेषताः</strong> जुनसुकै बेला जति पनि रकम राख्न र झिक्न सकिने ।</li>
                                    <li><strong>न्यूनतम मौज्दातः</strong> रु. २,००० ।– र रु. ५,००० ।–</li>
                                    <li><strong>ब्याजदरः</strong>७.००%</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                      <div class="col-md-6 col-sm-6 ">
                        <div class="icon-block icon-block-1">
                            <div class="icon-block-item">
                                <img src="images/icon4.png" alt="">
                            </div>
                            <div class="icon-block-body">
                                <h4 class="">आवधिक बचत</h4>
                                <ul class="service">
                                	<li><strong>विशेषताः</strong> निश्चित अवधि तोकि रकम जम्मा गर्न सकिन्छ र अवधि पूरा भइसकेपछि मात्र रकम झिक्न सकिने ।</li>
                                    <li><strong>न्यूनतम मौज्दातः</strong> रु. ५,००० ।–</li>
                                    <li><strong>ब्याजदरः</strong>७.००%</li>
                                </ul>
                            </div>
                        </div>
                    </div>              
                </div>
           
            
            
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("footer.php")?>