<?php include("header.php")?>
  
  <!-- MAIN CONTENT -->
  <div class="pg-opt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Board of Director</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                         <li><a href="about.php">About Us</a></li>
                        <li class="active">Board of Director</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
  <section class="slice white inset-shadow-1 bb">
    <div class="wp-section">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <?php include('sidebar.php')?>
          </div>
          <div class="col-md-9">
            
            <div class="row">
              
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/01Ambika Prasad Paudel.jpg" class="img-responsive"> </div>
                  <h2>Ambika Prasad Paudel<small>(Chairman)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/07Prakash Tiwari.jpg" class="img-responsive"> </div>
                  <h2>Prakash Tiwari<small>(Director)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/08Lila Nath Dhakal.jpg" class="img-responsive"> </div>
                  <h2>Lila Nath Dhakal<small>(Director)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/013Sagar Dhakal.jpg" class="img-responsive"> </div>
                  <h2>Sagar Dhakal<small>(Director-Public)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/017Nabaraj Nepal.jpg" class="img-responsive"> </div>
                  <h2>Nabraj Nepal<small>(Director-Public)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/018Pradhumna Kumar Bhattrai.jpg" class="img-responsive"> </div>
                  <h2>Pradhumna Kumar Bhattrai<small>(Professional Director)</small></h2>
                </div>
              </div>
              
             
              
            
              
          
              
              
              
              
              
              
              
            </div>
            
            
            
          </div>
          <div class="col-md-9 col-md-offset-3">
          	<nav aria-label="Page navigation">
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("footer.php")?>