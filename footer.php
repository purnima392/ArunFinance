<!-- FOOTER -->
  <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="footer-widget about-widget"><a href="index.html"><img src="images/logo1.png" alt="Awesome Image"></a>
              
              <div class="tel-box">
                <div class="icon-box"><i class="fa fa-map-marker"></i></div>
                <div class="text-box">
                  <p>Bhanu Chowk-6, Dharan, Sunsari</p>
                </div>
              </div>
              <div class="tel-box">
                <div class="icon-box"><i class="fa fa-phone"></i></div>
                <div class="text-box">
                  <p>(025) 533930 / 533931</p>
                </div>
              </div>
              
              <div class="tel-box">
                <div class="icon-box"><i class="fa fa-envelope"></i></div>
                <div class="text-box">
                  <p><a href="#">arunfinanceltd@gmail.com</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-12">
            <div class="footer-widget links-widget links-widget-pac">
              <div class="title">
                <h2>Explore</h2>
                <div class="decor-line"></div>
              </div>
            
                
                  <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Testimonials</a></li>
                    <li><a href="#">Contact Us</a></li>
                  </ul>
               
                
             
            </div>
          </div>
          <div class="col-md-4 col-sm-12">
            <div class="footer-widget subscribe-widget">
              <div class="title">
                <h2>Subscribe Newsletter</h2>
                <div class="decor-line"></div>
              </div>
              <form action="#">
                <p>Arun Finance Limited was registered in Company Registrar Office on 2051.11.12 as a public limited Company with limited liability on shares.</p>
                <div class="newsletter-widget">
                  <input type="text" placeholder="Enter your email address">
                  <button type="submit" class="thm-button"><i class="fa fa-envelope"></i></button>
                </div>
              </form>
              <ul class="social list-inline">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              </ul>
              
            </div>
          </div>
          
        </div>
      </div>
    </footer>
    <section class="footer-bottom">
      <div class="container clearfix">
        <div class="pull-left">
          <p>Copyright © 2017 Arun Finance Limited. All rights reserved. </p>
        </div>
        <div class="pull-right">
          <p>Created by:<a href="#"> Webpage Nepal Pvt. Ltd.</a></p>
        </div>
      </div>
    </section>
</div>

<!-- Essentials --> 
<script src="js/modernizr.custom.js"></script> 
<script src="assets/bootstrap/js/bootstrap.min.js"></script> 
<script src="js/jquery.mousewheel-3.0.6.pack.js"></script> 
<script src="js/jquery.easing.js"></script> 


<script src="js/jquery.stellar.js"></script> 

<!-- mobile nav --> 
<script src="assets/responsive-mobile-nav/js/jquery.dlmenu.js"></script> 
<script src="assets/responsive-mobile-nav/js/jquery.dlmenu.autofill.js"></script> 





<script src="assets/sky-forms/js/jquery.modal.js"></script> 

<!-- Assets --> 
<script src="assets/hover-dropdown/bootstrap-hover-dropdown.min.js"></script> 
<script src="assets/page-scroller/jquery.ui.totop.min.js"></script> 


<script src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script> 





<script src="assets/owl-carousel/owl.carousel.js"></script> 

<!-- Isotope -->
<script src="assets/isotope/isotope.min.js"></script>
<script src="assets/isotope/fit-columns.js"></script>
<!-- Sripts for individual pages, depending on what plug-ins are used --> 
<script src="assets/layerslider/js/greensock.js" type="text/javascript"></script> 
<script src="assets/layerslider/js/layerslider.transitions.js" type="text/javascript"></script> 
<script src="assets/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script> 
<script src="js/canvasjs.min.js" type="text/javascript"></script>
<!-- Initializing the slider --> 
<script>
    jQuery("#layerslider").layerSlider({
        pauseOnHover: true,
        autoPlayVideos: false,
        skinsPath: 'assets/layerslider/skins/',
        responsive: false,
        responsiveUnder: 1280,
        layersContainer: 1280,
        skin: 'borderlessdark3d',
        hoverPrevNext: true,
    });
</script> 

<!-- Boomerang App JS --> 
<script src="js/script.js"></script> 
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]--> 



</body>
</html>
