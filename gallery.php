<?php include("header.php")?>
  
  <!-- MAIN CONTENT -->
  <div class="pg-opt">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h2>Gallery</h2>
        </div>
        <div class="col-md-6">
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="active">Gallery</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <section class="slice white inset-shadow-1 bb">
    <div class="wp-section">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <?php include("sidebar.php")?>
          </div>
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-12"> 
                <!-- Masonry wrapper -->
                <div class="wp-masonry-wrapper wp-masonry-3-cols">
                  <div class="wp-masonry-gutter"></div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/finance.jpg" class="theater"> <img src="images/finance.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/img.jpg" class="theater"> <img src="images/img.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/finance.jpg" class="theater"> <img src="images/finance.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/deposite-banner.jpg" class="theater"> <img src="images/deposite-banner.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/img.jpg" class="theater"> <img src="images/img.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/finance.jpg" class="theater"> <img src="images/finance.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/deposite-banner.jpg" class="theater"> <img src="images/deposite-banner.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/img.jpg" class="theater"> <img src="images/img.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/finance.jpg" class="theater"> <img src="images/finance.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/deposite-banner.jpg" class="theater"> <img src="images/deposite-banner.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/img.jpg" class="theater"> <img src="images/img.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/finance.jpg" class="theater"> <img src="images/finance.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/deposite-banner.jpg" class="theater"> <img src="images/deposite-banner.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/img.jpg" class="theater"> <img src="images/img.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wp-masonry-block">
                    <div class="post-item style1">
                      <div class="post-content-wr">
                        <div class="post-meta-top">
                          <div class="post-image"> <a href="images/finance.jpg" class="theater"> <img src="images/finance.jpg" alt=""> </a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("footer.php")?>