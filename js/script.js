$(document).ready(function() {

	// Mobile Nav (Second method besides Bootstrap standrd menu) - Populating mobile nav list
	var navbarContainer = $('.navbar-nav');
	var linkTextLevel1;
	var linkTextLevel2;
	var linkTextLevel3;
	var navList;
	var navListEnd = '</ul>';

	// Get first level items
	$('.navbar-nav > li.dropdown').each(function(index) {
		// Meganav list items
		if($(this).hasClass('dropdown-meganav')){
			linkTextLevel1 = $(this).find('a:first').text();
			linkUrlLevel1 = $(this).find('a:first').attr('href');
			navList = '<li><a href="'+ linkUrlLevel1 +'">'+ linkTextLevel1 +'</a>';

			// Get second level items
			if($(this).find('.mega-nav-section').length > 0){
				navList = navList + '<ul class="dl-submenu">';

				$(this).find('.mega-nav-section').each(function(index){
					linkTextLevel2 = $(this).find('.mega-nav-section-title').text();
					linkUrlLevel2 = $(this).find('a:first').attr('href');
					navList = navList + '<li><a href="'+ linkUrlLevel2 +'">'+ linkTextLevel2 +'</a>';

					// Get third level items
					if($(this).find('.mega-nav-ul').length > 0){
						navList = navList + '<ul class="dl-submenu">';
						
						$(this).find('.mega-nav-ul > li').each(function(index){
							linkTextLevel3 = $(this).find('a').text();
							linkUrlLevel3 = $(this).find('a:first').attr('href');
							navList = navList + '<li><a href="'+ linkUrlLevel3 +'">'+ linkTextLevel3 +'</a></li>';
						});

						navList = navList + navListEnd;
						navList = navList + '</li>';
					} 
				});
			}

		}

		// Normal nav list items
		if(!$(this).hasClass('dropdown-search') && !$(this).hasClass('dropdown-meganav')){
			linkTextLevel1 = $(this).find('a:first').text();
			linkUrlLevel1 = $(this).find('a:first').attr('href');
			navList = '<li><a href="'+ linkUrlLevel1 +'">'+ linkTextLevel1 +'</a>';

			// Get second level items
			if($(this).find('ul > li').length > 0){
				navList = navList + '<ul class="dl-submenu">';

				$(this).children().children().each(function(index){
					linkTextLevel2 = $(this).find('a:first').text();
					linkUrlLevel2 = $(this).find('a:first').attr('href');
					navList = navList + '<li><a href="'+ linkUrlLevel2 +'">'+ linkTextLevel2 +'</a>';

					// Get third level items
					if($(this).find('ul > li').length > 0){
						navList = navList + '<ul class="dl-submenu">';
						
						$(this).children().children().each(function(index){
							linkTextLevel3 = $(this).find('a').text();
							linkUrlLevel3 = $(this).find('a:first').attr('href');
							navList = navList + '<li><a href="'+ linkUrlLevel3 +'">'+ linkTextLevel3 +'</a></li>';
						});

						navList = navList + navListEnd;
						navList = navList + '</li>';
					} 
				});
			} 		
		}

		navList = navList + navListEnd;
		navList = navList + '</li>';

		// Append and create list menu
		$('#dl-menu > ul.dl-menu').append(navList + '</ul>');		
	});
	// Initializing mobile menu
	$('#dl-menu').dlmenu({
        animationClasses:{
        	classin: 'dl-animate-in-2', 
        	classout: 'dl-animate-out-2' 
        }
    });
    // Bootstrap - Dropdown Hover
   	function allowDropdownHover() {
   		if($(window).width() > 767){
	    	$('.dropdown-toggle').dropdownHover();
	    }	
    }
    allowDropdownHover();
    window.onresize = allowDropdownHover; // Call the function on resize

	//Carousels
	$('.carousel').carousel({
		interval: 5000,
		pause	: 'hover'
	});
	// Owl carousel
	if($(".owl-carousel-single").length > 0){
		$(".owl-carousel-single").owlCarousel({
			//items : 4,
			lazyLoad : true,
			pagination : false,
			autoPlay: 10000,
			singleItem: true,
			stopOnHover: true
		}); 
	}
	if($(".owl-carousel-2").length > 0){
		// Owl Carousel
		$(".owl-carousel-2").owlCarousel({
			items : 2,
			lazyLoad : true,
			pagination : false,
			autoPlay: 10000,
			stopOnHover: true
		}); 
	}
	if($(".owl-carousel-3").length > 0){
		// Owl Carousel
		$(".owl-carousel-3").owlCarousel({
			items : 3,
			lazyLoad : true,
			pagination : false,
			autoPlay: 10000,
			stopOnHover: true
		}); 
	}
	if($(".owl-carousel-4").length > 0){
		// Owl Carousel
		$(".owl-carousel-4").owlCarousel({
			items : 4,
			lazyLoad : true,
			pagination: false,			
			autoPlay: 10000,
			stopOnHover: true
		}); 
	}
	if($(".owl-carousel-5").length > 0){
		// Owl Carousel
		$(".owl-carousel-5").owlCarousel({
			items : 5,
			lazyLoad : true,
			pagination : false,
			autoPlay: 10000,
			stopOnHover: true
		}); 
	}
	
	// Fancybox
	$(".theater").fancybox();
	// Fancybox	
	$(".ext-source").fancybox({
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'autoScale'     	: false,
		'type'				: 'iframe',
		'width'				: '50%',
		'height'			: '60%',
		'scrolling'   		: 'no'
	});
	// Stellar JS
	$(".prlx-bg").stellar();
	// Isotope Masonry
	if($('.wp-masonry-wrapper').length > 0){
		$('.wp-masonry-wrapper').isotope({
		    itemSelector: '.wp-masonry-block',
		    masonry: {
		      columnWidth: '.wp-masonry-block',
		      gutter: '.wp-masonry-gutter'
		    }
		  });
	}
	
	// Scroll to top
	$().UItoTop({ easingType: 'easeOutQuart' });
	
	// Search
	if($('#btnSearch').length > 0){
		// Search function
		$("#btnSearch").click(function(){
			if($("#divSearch").is(":visible")){
				$("#divSearch").removeClass('animated slideInDown');
				$("#divSearch").addClass('animated slideOutUp');
				$('#divSearch').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
					$(this).hide();
					$("#divSearch").removeClass('animated slideOutUp');
				});
			} else {
				$("#divSearch").show().addClass('animated slideInDown');
			}
			return false;	
		});
		$("#cmdCloseSearch").click(function(){
			if($("#divSearch").is(":visible")){
				$("#divSearch").removeClass('animated slideInDown');
				$("#divSearch").addClass('animated slideOutUp');
				$('#divSearch').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
					$(this).hide();
					$("#divSearch").removeClass('animated slideOutUp');
				});
			}
		});
		
		// Keyboard shortcuts
		$('html').keyup(function(e){
			if(e.keyCode == 27){
				if($("#divSearch").is(":visible")){
					$("#divSearch").removeClass('animated slideInDown');
					$("#divSearch").addClass('animated slideOutUp');
					$('#divSearch').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
						$(this).hide();
						$("#divSearch").removeClass('animated slideOutUp');
					});
				}
			}
		});
	}
	// Hover direction animation	
	$(".animate-hover-slide-4 .figure").each( function(){ 
		$(this).hoverdir({
			hoverDelay: 50,
			inverse: 	false
		}); 
	});
	// Custom animations
	$(".animate-click").click(function(){
		var animateIn = $(this).data("animate-in");
		var animateOut = $(this).data("animate-out");
		var animatedElement = $(this).find(".animate-wr");

		console.log(animateIn+'-'+animateOut);

		if(animateIn != undefined){
			console.log('incepem animatia');
			if(animatedElement.is(":hidden")){
				console.log('start IN');
				animatedElement.addClass(animateIn);
				animatedElement.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
					animatedElement.removeClass(animateIn);
				});
			}
		}
	});
	
	$(".animate-hover").hover(function(){
		var animation = $(this).data("animate-in");
		if(animation != undefined || animation != ""){
			$(this).find(".animate-wr").addClass(animation);
		}
	});

	$(".animate-click").click(function(){
		var animation = $(this).data("animate-in");
		if(animation != undefined || animation != ""){
			$(this).find(".animate-wr").addClass(animation);
		}
	});

	// Main nav for mobiles - left
	$(".navbar-toggle-mobile-nav, #btnHideMobileNav").click(function(){
		if($("#navMobile").is(":visible")){
			$("#navMobile").removeClass('animated bounceInLeft');
			$("#navMobile").addClass('animated bounceOutLeft');
			$('#navMobile').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
				$(this).hide();
				$("#navMobile").removeClass('animated bounceOutLeft');
			});
			$("body").removeClass("nav-menu-in");
		}
		else{
			$("#navMobile").show().addClass('animated bounceInLeft');
			$("body").addClass("nav-menu-in");
		}
		return false;	
	});	
 
	
    // Optional filters
    $("#btnToggleOptionalFIlters").click(function(){
    	var animateIn = $(this).data("animate-in");
	    var animateOut = $(this).data("animate-out");

    	if($(this).hasClass("opened")){
    		$(".hidden-form-filters").addClass('hide');
    		$(this).removeClass("opened");
    	} else{
	    	$(this).addClass("opened");
	    	$(".hidden-form-filters").removeClass("hide");
	    }
	    return false;
    });

    // Layer Slider Dynamic- Set height to fit navbar
    if($(".layer-slider-dynamic").length > 0){
    	layerSliderDynamic();
    }

    // Layer Slider Fullsize
    if($(".layer-slider-fullsize").length > 0){
    	layerSliderFullsize();
    }

    // Window resize events
    $(window).resize(function() {
    	if($(".layer-slider-dynamic").length > 0){
	    	layerSliderDynamic();
	    }
		if($(".layer-slider-fullsize").length > 0){
	    	layerSliderFullsize();
	    }
	});
    
    // Functions
    function layerSliderDynamic(){
    	var windowHeight = $(window).height();
    	var headerHight = $("#divHeaderWrapper").height();
    	var newSliderHeight = windowHeight - headerHight;
    	$("#layerslider").css({"height": newSliderHeight + "px"});
    }
    function layerSliderFullsize(){
    	var windowHeight = $(window).height();
    	$("#layerslider").css({"height": windowHeight + "px"});
    }


    var screenRes = $(window).width(),
        screenHeight = $(window).height(),
        html = $('html');

    // IE<8 Warning
    if (html.hasClass("ie8")) {
        $("body").empty().html('Please, Update your Browser to at least IE9');
    }

    // Disable Empty Links
    $("[href=#]").click(function(event){
        event.preventDefault();
    });

    // Body Wrap
    $(".body-wrap").css("min-height", screenHeight);
    $(window).resize(function() {
        screenHeight = $(window).height();
        $(".body-wrap").css("min-height", screenHeight);
    });

    // styled Select, Radio, Checkbox
    if ($("select").hasClass("select_styled")) {
        cuSel({changedEl: ".select_styled", visRows: 8, scrollArrows: true});
    }

    
    // Tooltip & Popover
    $('.ttip').tooltip({
    	placement: $(this).data('toggle'),
    	html: true
    });

    $('.pop').popover({
    	placement: 'right',
    	html: true
    });

   
});
window.onload = function () {
			var chart = new CanvasJS.Chart("chartContainer", {
				title: {
					text: "बचत तथा कर्जाको बिबरण"
				},
				data: [{
					type: "column",
					dataPoints: [
					  { x: 10, y: 750 },
					  { x: 20, y: 714 },
					  { x: 30, y: 320 },
					  { x: 40, y: 560 },
					  { x: 50, y: 430 },
					  { x: 60, y: 500 },
					  { x: 70, y: 480 },
					  { x: 80, y: 280 },
					  { x: 90, y: 410 },
					  { x: 100, y: 500 },
					]
				}, {
					type: "column",
					dataPoints: [
					  { x: 10, y: 250 },
					  { x: 20, y: 414 },
					  { x: 30, y: 919 },
					  { x: 40, y: 967 },
					  { x: 50, y: 678 },
					  { x: 60, y: 878 },
					  { x: 70, y: 787 },
					  { x: 80, y: 780 },
					  { x: 90, y: 803 },
					  { x: 100, y: 330 },
					]
				}, {
					
				}]
			});
			chart.render();
		}
		var chart = new CanvasJS.Chart("piechartContainer",
	{
		theme: "theme2",
		title:{
			text: "कर्जा संरचना"
		},
		data: [
		{
			type: "pie",
			showInLegend: true,
			toolTipContent: "{y} - #percent %",
			
			legendText: "{indexLabel}",
			dataPoints: [
				{  y: 1.92, indexLabel: "हयर पर्चेज" },
				{  y: 1.34, indexLabel: "महिला लघुबित्त" },
				{  y: 9.75, indexLabel: "व्यवसाय" },
				{  y: 71.27, indexLabel: "व्यक्तिगत"},
				{  y: 13.19, indexLabel: "आवधिक धितो" },
				{  y: 1.78, indexLabel: "आकस्मिक"},
				{  y: 0.75, indexLabel: "इजि"}
			]
		}
		]
	});
	chart.render();