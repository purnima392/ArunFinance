<?php include("header.php")?>
  
  <!-- MAIN CONTENT -->
  <div class="pg-opt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Branch Network</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        
                        <li class="active">Branch Network</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
  <section class="slice white inset-shadow-1 bb">
    <div class="wp-section">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <?php include('sidebar.php')?>
          </div>
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-6">
              	<div class="contact_box">
                	<h3>Head Office</h3>
                    <p><i class="fa fa-map-marker"></i>Bhanu Chowk-6, Dharan, Sunsari</p>
                    <p><i class="fa fa-phone"></i>025-533930/533931</p>
                     <p><i class="fa fa-envelope"></i>info@arunfinance.com</p>
                </div>
              </div>
             <div class="col-md-6">
              	<div class="contact_box">
                	<h3>Hetauda Office</h3>
                    <p><i class="fa fa-map-marker"></i>Bhanu Chowk-6, Dharan, Sunsari</p>
                    <p><i class="fa fa-phone"></i>057-526813/526814</p>
                     <p><i class="fa fa-envelope"></i>info@arunfinance.com</p>
                </div>
              </div>
              <div class="col-md-6">
              	<div class="contact_box">
                	<h3>Khadichaur Office</h3>
                    <p><i class="fa fa-map-marker"></i>Bhanu Chowk-6, Dharan, Sunsari</p>
                    <p><i class="fa fa-phone"></i>011-482143/482144</p>
                     <p><i class="fa fa-envelope"></i>info@arunfinance.com</p>
                </div>
              </div>
            </div>
          
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("footer.php")?>