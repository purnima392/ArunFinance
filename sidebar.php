<aside class="sidebar">
              <div class="block">
                <div class="section-title-wr">
                  <h3 class="section-title left"><i class="fa fa-calendar-o"></i><span>News & Events</span></h3>
                </div>
                <ul class="list">
                  <li><a href="#">Fortitor Posuere & Praesent Metus Ins</a>
                  <span class="date">November, 05, 2017</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. At vero eos et accusamus et iusto odio </p>
                    <a href="newsDetail.php" class="read-btn">Read More</a> </li>
                  <li><a href="#">Fortitor Posuere & Praesent </a>
                  <span class="date">November, 05, 2017</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. At vero eos et accusamus et iusto odio </p>
                    <a href="newsDetail.php" class="read-btn">Read More</a> </li>
                </ul>
              </div>
              <div class="block">
                <div class="section-title-wr">
                  <h3 class="section-title left"><i class="fa fa-bank"></i><span>Deposit Scheme</span></h3>
                </div>
                <ul class="list btn-list">
                  <li><a href="#">Arun Saving</a><span class="pull-right badge">6.50 %</span></li>
                  <li><a href="#">Khutruke Saving</a><span class="pull-right badge">6.50 %</span></li>
                  <li><a href="#">Special Saving</a><span class="pull-right badge">8.50 %</span></li>
                  <li><a href="#">Uddhyami Saving</a><span class="pull-right badge">7.50 %</span> </li>
				  <li><a href="#">Remittance Saving</a><span class="pull-right badge">8.00 %</span> </li>
				  <li><a href="#">Karmachari Saving</a><span class="pull-right badge">8.25 %</span> </li>
				  <li><a href="#">Didi bahini Saving</a><span class="pull-right badge">8.50 %</span> </li>
				  <li><a href="#">Institutional Account</a><span class="pull-right badge">6.50 %</span> </li>
				  <li><a href="#">Special Saving</a><span class="pull-right badge">8.50 %</span> </li>
				  
                </ul>
              </div>
            
            </aside>