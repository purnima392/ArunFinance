<?php include("header.php")?>
  
  <!-- MAIN CONTENT -->
  <div class="pg-opt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Our Team</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                         <li><a href="about.php">About Us</a></li>
                        <li class="active">Our Team</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
  <section class="slice white inset-shadow-1 bb">
    <div class="wp-section">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <?php include('sidebar.php')?>
          </div>
          <div class="col-md-9">
            
            <div class="row">
              
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/014Arjun Pokhrel.jpg" class="img-responsive"> </div>
                  <h2>Arjun Pokharel<small>(Branch Manager-Hetauda)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/010Karna photo.jpg" class="img-responsive"> </div>
                  <h2>Samjhana Shrestha<small>(Branch Manager-Khadichaur)</small></h2>
                </div>
              </div>
             </div>
            
            
            
          </div>
          <div class="col-md-9 col-md-offset-3">
          	<nav aria-label="Page navigation">
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("footer.php")?>