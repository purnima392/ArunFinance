<?php include("header.php")?>
  <!-- Importing slider content -->
  <section id="slider-wrapper" class="layer-slider-wrapper layer-slider-dynamic">
    <div id="layerslider" style="width:100%;height:520px;"> 
      <!-- Slide 2 -->
      <div class="ls-slide" data-ls="transition2d:1;timeshift:-1000;"> 
        <!-- slide background --> 
        <img src="images/arun-slider.jpg" class="ls-bg" style="width: 100%;" alt="Slide background"/> </div>
      <!-- Slide 1 -->
      <div class="ls-slide" data-ls="transition2d:1;timeshift:-1000;"> 
        <!-- slide background --> 
        <img src="images/deposite-banner.jpg" class="ls-bg" alt="Slide background"/> </div>
    </div>
  </section>
  
  <!-- MAIN CONTENT -->
  <section class="service">
    <div class="container">
      <div class="title">
        <h2>Loan & Interest <span>Rates</span></h2>
        <p class="lead">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque </p>
      </div>
      <div class="service-list">
        <div class="service_item noleft-border"> <i class="icon"><img src="images/2.png" alt=""></i>
          <h3><a href="#">Home Loan <span>15-18%</span></a></h3>
        </div>
        <div class="service_item"> <i class="icon"><img src="images/BUYER.png" alt=""></i>
          <h3><a href="#">Business Loan <span>15-18%</span></a></h3>
        </div>
        <div class="service_item"> <i class="icon"><img src="images/870932-200.png" alt=""></i>
          <h3><a href="#">Auto Loan <span>15-18%</span></a></h3>
        </div>
        <div class="service_item noleft-border nobottom-border"> <i class="icon"><img src="images/environment-icon.png" alt=""></i>
          <h3><a href="#">Agriculture Loan <span>15-18%</span></a></h3>
        </div>
        <div class="service_item nobottom-border"> <i class="icon"><img src="images/icon-quarterly-provisional-figures.png" alt=""></i>
          <h3><a href="#">DEPRIVED SECTOR LOAN <span>15-18%</span></a></h3>
        </div>
        <div class="service_item nobottom-border"> <i class="icon"><img src="images/hand-holding-dollar-sign.png" alt=""></i>
          <h3><a href="#">PERSONAL LOAN<span>15-18%</span></a></h3>
        </div>
      </div>
    </div>
  </section>
  <section class="intro light-bg">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="inner-title">
            <h2>Welcome to <span>Arun Finance Limited</span></h2>
            <p class="lead">"A National Level Finance Company in the market operating since more that two decade promoted by Hathway Group"</p>
          </div>
          <div class="border-line"></div>
          <p><b>Arun Finance Limited</b> was registered in Company Registrar Office on 2051.11.12 as a public limited Company with limited liability on shares. It received permission from NRB as of “C” Class Financial Institution on 2054.04.02 as a National level Finance.</p>
          <a href="#" class="btn btn-primary">Read More</a> </div>
        <div class="col-md-6"> <img src="images/finance.jpg" alt=""> </div>
      </div>
    </div>
  </section>
  <section class="sec-pad graph">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div id="chartContainer" style="height: 350px; width: 100%;"></div>
        </div>
        <div class="col-md-6">
          <div id="piechartContainer" style="height: 350px; width: 100%;"></div>
        </div>
      </div>
    </div>
  </section>
  <hr/>
  <section class="scheme">
    <div class="container">
      <div class="inner-title">
        <h2>Deposit Scheme and <span>Interest Rates</span></h2>
        <div class="border-line"></div>
      </div>
      <div class="scheme-list row">
        <div class="col-md-6">
          <div class="scheme_item">
            <h4>Arun Finance Scheme</h4>
            <ul class="list clearfix">
              <li><a href="#">Arun Saving<span class="pull-right badge">6.50 %</span></a></li>
              <li><a href="#">Khutruke Saving<span class="pull-right badge">6.50 %</span></a></li>
              <li><a href="#">Special Saving<span class="pull-right badge">8.50 %</span></a></li>
              <li><a href="#">Uddhyami Saving<span class="pull-right badge">7.50 %</span></a></li>
              <li><a href="#">Remittance Saving<span class="pull-right badge">8.00 %</span></a></li>
              <li><a href="#">Karmachari Saving<span class="pull-right badge">8.25 %</span></a></li>
              <li><a href="#">Didi bahini Saving<span class="pull-right badge">8.50 %</span></a></li>
              <li><a href="#">Institutional Account<span class="pull-right badge">6.50 %</span></a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-6">
          <div class="scheme_item">
            <h4>Fixed Deposit</h4>
            <ul class="list clearfix">
              <li><a href="#">3 Months FD <span class="pull-right badge">10 %</span></a></li>
              <li><a href="#">6 Months FD<span class="pull-right badge">10.50 %</span></a></li>
              <li><a href="#">9 Months FD<span class="pull-right badge">11 %</span></a></li>
              <li><a href="#">1 – 2 Years FD<span class="pull-right badge">13 %</span></a></li>
              <li><a href="#">Dirghayu Bhawa FD (3 – 24 Months) <span class="pull-right badge">10- 13 %</span></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="full-img-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-7">
          <div class="left-content">
            <div class="sec-title">
              <h2>Why Arun Finance?</h2>
              <div class="decor-line"></div>
            </div>
            <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
            <br>
            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <br>
            <ul>
              <li>व्यक्तिहरुमा सहकारिताको भावना जगाई बचत गर्ने चेतना एवम् बानीको विकास गर्न ।</li>
              <li>समाजमा निष्क्रिय रहेका स–साना पुँजीलाई एकत्रित गरी उत्पादनमुखी कार्यमा परिचालन गर्न ।</li>
              <li>आपूmमा अन्तरनिहित सीप र क्षमताको विकास गरी आत्मविश्वास बढाई स्वावलम्बी बन्न </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--<section class="sec-pad team-bg">
    <div class="container">
      <div class="sec-title">
        <h2>सञ्चालक समिति २०७२–२०७५</h2>
        <div class="decor-line"></div>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <div class="single-team-member">
            <div class="img-box"><img src="images/team-1.jpg" alt="Awesome Image">
              <div class="overlay">
                <div class="box">
                  <div class="content">
                    <ul class="social list-inline text-center">
                      <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                      <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="text-box">
              <h4>नारायण पौडेल</h4>
              <p>अध्यक्ष</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="single-team-member">
            <div class="img-box"><img src="images/team-1.jpg" alt="Awesome Image">
              <div class="overlay">
                <div class="box">
                  <div class="content">
                    <ul class="social list-inline text-center">
                      <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                      <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="text-box">
              <h4>Chelsea Deitch</h4>
              <p>Director of Design</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="single-team-member">
            <div class="img-box"><img src="images/team-1.jpg" alt="Awesome Image">
              <div class="overlay">
                <div class="box">
                  <div class="content">
                    <ul class="social list-inline text-center">
                      <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                      <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="text-box">
              <h4>Ralph Marino</h4>
              <p>Business Development</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="single-team-member">
            <div class="img-box"><img src="images/team-1.jpg" alt="Awesome Image">
              <div class="overlay">
                <div class="box">
                  <div class="content">
                    <ul class="social list-inline text-center">
                      <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                      <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="text-box">
              <h4>Holly Hill</h4>
              <p>Account Director</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>-->
  <section class="sec-pad sewa">
    <div class="container">
      <div class="sec-title">
        <h2>Partner Associated </h2>
        <div class="decor-line"></div>
      </div>
      <div class="owl-carousel-5">
        <div class="client"> <a href="#"><img src="images/westernunion.png" alt=""></a> </div>
        <div class="client"> <a href="#"><img src="images/city-express-logo-500x189.jpg" alt=""></a> </div>
        <div class="client"> <a href="#"><img src="images/himalremit.jpg" alt=""></a> </div>
        <div class="client"> <a href="#"><img src="images/moneygram.jpg" alt=""></a> </div>
        <div class="client"> <a href="#"><img src="images/IME.jpg" alt=""></a> </div>
        <div class="client"> <a href="#"><img src="images/prabhu.jpg" alt=""></a> </div>
        <div class="client"> <a href="#"><img src="images/easylink.jpg" alt=""></a> </div>
        <div class="client"> <a href="#"><img src="images/ipay.jpg" alt=""></a> </div>
      </div>
    </div>
  </section>
  <?php include("footer.php")?>