<?php include("header.php")?>
  
  <!-- MAIN CONTENT -->
  <div class="pg-opt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Downloads</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        
                        <li class="active">Downloads</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
  <section class="slice white inset-shadow-1 bb animate-hover-slide">
    <div class="wp-section">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <?php include('sidebar.php')?>
          </div>
          <div class="col-md-9">
            <div class="section-title-wr">
                    <h3 class="section-title left">
                        <span>Download Required Documents</span>
                        
                    </h3>
                </div>
               <div class="button-wrapper">
               	<a href="pdf/0-06-Oct-2017-04-10-52Unaudited 2074 Ashadh.pdf" class="btn btn-base btn-lg"><i class="fa fa-download"></i>Unaudited Financial <span>Result</span></a>
                	<a href="images/1-19-Sep-2017-02-09-58diposite-scheme.pdf" class="btn btn-base btn-lg"><i class="fa fa-download"></i>Deposit Scheme and Interest <span>Rates</span></a>
                    	<a href="images/0-19-Sep-2017-02-09-58Loan-and-interest-rate.pdf" class="btn btn-base btn-lg"><i class="fa fa-download"></i>Loan & Interest<span>Rates</span></a>
                        	
                            	
               </div> 
               
                
                
           
            
            
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("footer.php")?>