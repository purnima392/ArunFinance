<?php include("header.php")?>
  
  <!-- MAIN CONTENT -->
  <div class="pg-opt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Corporate Profile</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        
                        <li class="active">Corporate Profile</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
  <section class="slice white inset-shadow-1 bb">
    <div class="wp-section">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <?php include('sidebar.php')?>
          </div>
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-7">
                <div class="section-title-wr-top">
                  <h3 class="section-title left"><span>Welcome To</span> Arun Finance Limited </h3>
                  <div class="border-line"></div>
                </div>
                <p>Arun Finance Limited was registered in Company Registrar Office on 2051.11.12 as a public limited Company with limited liability on shares. It received permission from NRB as of “C” Class Financial Institution on 2054.04.02 as a National level Finance.</p>
                <div class="subtitle"><h4>Share Capital Composition</h4></div>
                <p>The Authorized Share Capital of the Finance is NPR 1,000,000,000.00 (One Arab Nepalese Rupees) divided into 10,000,000.00 shares of NPR 100.00 each. Issued and paid-up capital of the Finance is NPR 30,000,000.00. Promoter of the Finance holds 60% of the total of the total share capital of the Finance and remaining portion (i.e. 40%) is
held by the general public.</p>
                </div>
              <div class="col-md-5"><img src="images/Finance-PNG-Pic.png" alt=""></div>
            </div>
            <hr/>
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
            	<tr>
                	<th>Shareholders</th>
                    <th>Percentage</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td>Promoters</td>
                    <td>60%	</td>
                    <td>18,00,00,000</td>
                </tr>
                <tr>
                	<td>General Public</td>
                    <td>40%</td>
                    <td>12,00,00,000</td>
                </tr>
                <tr>
                	<td>Calls in Advance	</td>
                    <td></td>
                    <td>73,360,000</td>
                </tr>
                
                <tr>
                	<td colspan="2" class="text-right"><b>Total</b></td>
                    <td>30,00,00,000.00</td>
                </tr>
                
                
                
                
            </tbody>
          </table>
      </div>
                
              </div>
              
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("footer.php")?>