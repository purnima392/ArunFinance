<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="index, follow">
<title>Arun Finance Limited</title>

<!-- Essential styles -->
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="css/stroke-gap-icon.css" type="text/css">
<link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen">

<!--styles -->
<link type="text/css" href="css/style.css" rel="stylesheet" media="screen">
<link type="text/css" href="css/custom.css" rel="stylesheet" media="screen">
<link type="text/css" href="css/responsive.css" rel="stylesheet" media="screen">
<!-- Favicon -->


<!-- Assets -->
<link rel="stylesheet" href="assets/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="assets/owl-carousel/owl.theme.css">
<link rel="stylesheet" href="assets/sky-forms/css/sky-forms.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" href="assets/sky-forms/css/sky-forms-ie8.css">
    <![endif]-->

<!-- Required JS -->
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>

<!-- Page scripts -->
<link rel="stylesheet" href="assets/layerslider/css/layerslider.css" type="text/css">
</head>
<body>

<!-- MAIN WRAPPER -->
<div class="body-wrap">

<!-- HEADER -->
<div id="divHeaderWrapper">
  <header class="clearfix">
    <div class="container">
      <div class="logo pull-left">
        <h2><a href="#"><img src="images/logo1.png" alt=""></a></h2>
      </div>
      <div class="header-right pull-right hidden-xs">
        <ul>
          <li>
            <div class="single-header-right">
              <div class="icon-box"><i class="icon icon-Pointer"></i></div>
              <div class="text-box">
                <p>Bhanu Chowk-6,  <br>
                  Dharan, Sunsari </p>
              </div>
            </div>
          </li>
          <li>
            <div class="single-header-right">
              <div class="icon-box"><i class="icon icon-Phone2"></i></div>
              <div class="text-box">
                <p>025-533930 / 533931 <br>
                  arunfinanceltd@gmail.com</p>
              </div>
            </div>
          </li>
          <li>
            <div class="single-header-right">
              <div class="icon-box"><i class="icon icon-Timer"></i></div>
              <div class="text-box">
                <p>Sun - Fri 10.00 A.M - 5.00 P.M <br>
                  Saturday Closed</p>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </header>
  <!--Navigation Start-->
  <div class="navigation">
    <div class="container">
      <div class="navbar navbar-wp navbar-arrow mega-nav" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <i class="fa fa-bars icon-custom"></i> </button>
          <a class="navbar-brand" href="index.php" title="Jaya Manakamana Saving & Credit Co-operative Ltd">Menu</a> </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"> <a href="index.php" >Home</a> </li>
            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us</a>
              <ul class="dropdown-menu">
                <li><a href="about.php">Corporate Profile</a></li>
                <li><a href="#">Mission and Vision</a></li>
                <li><a href="board.php">Board of Director</a></li>
                <li><a href="management-team.php">Management Team</a></li>
                <li><a href="our-team.php">Our Team</a></li>
                <li><a href="#">Organization Structure</a></li>
                <li><a href="#">Future Plan</a></li>
              </ul>
            </li>
            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products & Services</a>
              <ul class="dropdown-menu">
                <li><a href="loan-interset.php">Loan & Interest</a></li>
                <li><a href="deposit-scheme.php">Deposit & Interest</a></li>
                <li><a href="#">Remittance</a></li>
                <li><a href="#">ABBS</a></li>
               
              </ul>
            </li>
            <li> <a href="branch-network.php">Branch Network</a> </li>
            <li> <a href="datareport.php">Data & Report</a> </li>
            <li> <a href="newsList.php">Notices/News</a> </li>
            <li> <a href="gallery.php">Gallery</a> </li>
            <li> <a href="career.php">Career</a> </li>
            <li> <a href="contact.php">Contact Us</a> </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
