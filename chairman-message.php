<?php include("header.php")?>
  
  <!-- MAIN CONTENT -->
  <div class="pg-opt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Chairmam Message</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about.php">About Us</a></li>
                        <li class="active">Chairmam Message</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
  <section class="slice white inset-shadow-1 bb">
    <div class="wp-section">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <aside class="sidebar">
              <div class="block">
                <div class="section-title-wr">
                  <h3 class="section-title left"><i class="fa fa-calendar-o"></i><span>News & Events</span></h3>
                </div>
                <ul class="list">
                  <li><a href="#">सोह्रौं वार्षिक साधारण सभा</a>
                    <p>पोखरा,  ४ मंसिर २०७३| जय मनकामना बचत तथा ऋण सहकारी संस्था लि.को संस्थाका अध्यक्ष नारायण पौडेलको अध्यक्षतामा पोखरामा शनिबार सम्पन्न भयो ।</p>
                    <a href="#" class="read-btn">Read More</a> </li>
                  <li><a href="#">सोह्रौं वार्षिक साधारण सभा</a>
                    <p>पोखरा,  ४ मंसिर २०७३| जय मनकामना बचत तथा ऋण सहकारी संस्था लि.को संस्थाका अध्यक्ष नारायण पौडेलको अध्यक्षतामा पोखरामा शनिबार सम्पन्न भयो ।</p>
                    <a href="#" class="read-btn">Read More</a> </li>
                </ul>
              </div>
              <div class="block">
                <div class="section-title-wr">
                  <h3 class="section-title left"><i class="fa fa-bank"></i><span>Deposit Scheme</span></h3>
                </div>
                <ul class="list btn-list">
                  <li><a href="#"> नारी आवधिक <span>बचत </span></a> </li>
                  <li><a href="#">बाल शैक्षिक<span>बचत </span></a> </li>
                  <li><a href="#">मासिक <span>बचत </span></a> </li>
                  <li><a href="#">मुद्दती <span>खाता </span></a> </li>
                </ul>
              </div>
              <div class="block">
                <div class="section-title-wr">
                  <h3 class="section-title left"><i class="fa fa-facebook"></i><span>Follow Us Facebook</span></h3>
                </div>
                
                
              </div>
            </aside>
          </div>
          <div class="col-md-9">
            <div class="section-title-wr">
                  <h3 class="section-title left"><span>Chairman Message</span></h3>
                 
                </div>
                
                 
                  
           
            
            
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("footer.php")?>