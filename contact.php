<?php include("header.php")?>
  
  <!-- MAIN CONTENT -->
  <div class="pg-opt">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h2>Contact Us</h2>
        </div>
        <div class="col-md-6">
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="active">Contact Us</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <section class="slice white inset-shadow-1 bb animate-hover-slide">
    <div class="wp-section">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
		  <?php include('sidebar.php')?>
          </div>
          <div class="col-md-9">
            <section class="slice no-padding">
              <div id="mapCanvas" class="map-canvas no-margin">
				  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3560.9174633954854!2d87.2829826144235!3d26.810756570927587!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ef41993e73c6b1%3A0x88904991786d214c!2sArun+Finance+Limited!5e0!3m2!1sen!2snp!4v1509880617254" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				 </div>
            </section>
            <section class="slice bg-white">
              <div class="w-section inverse">
                <div class="row">
                  
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="section-title-wr">
                          <h3 class="section-title left"><span>Arun Finance Limited</span></h3>
                        </div>
                        <div class="contact-info">
                          <ul>
                            <li><i class="fa fa-map-marker"></i>
                              <p>	
	
Bhanu Chowk-6,
Dharan, Sunsari</p>
                            </li>
                            
                            <li> <i class="fa fa-phone"></i>
                              <p><b>Head Office:</b>025-533930/533931</p>
							  <p><b>Hetauda:</b>057-526813/526814</p>
							  <p><b>Khadichaur:</b>011-482143/482144</p>
                            </li>
                            <li> <i class="fa fa-envelope"></i>
                              <p>arunfinanceltd@gmail.com</p>
                            </li>
                            <li> <i class="fa fa-globe"></i>
                              <p>	
<a href="http://www.arunfinance.com">www.arunfinance.com</a></p>
                            </li>
                          </ul>
                        </div>
                      </div>
					  <div class="col-md-6"><form class="form-light mt-20" role="form">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Your name">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" placeholder="Email address">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" placeholder="Phone number">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Subject</label>
                                <input type="text" class="form-control" placeholder="Subject">
                            </div>
                            <div class="form-group">
                                <label>Message</label>
                                <textarea class="form-control" id="message" placeholder="Write you message here..." style="height:100px;"></textarea>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="reset" class="btn btn-light">Reset</button>
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-base btn-icon btn-icon-right btn-fly pull-right">
                                        <span>Send message</span>
                                    </button>
                                </div>
                            </div>
                        </form></div>
                    </div>
                    
                    
                    
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("footer.php")?>