<?php include("header.php")?>
  
  <!-- MAIN CONTENT -->
  <div class="pg-opt">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h2>Committee</h2>
        </div>
        <div class="col-md-6">
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li><a href="about.php">About Us</a></li>
            <li class="active">Committee</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <section class="slice white inset-shadow-1 bb">
    <div class="wp-section">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <aside class="sidebar">
              <div class="block">
                <div class="section-title-wr">
                  <h3 class="section-title left"><i class="fa fa-calendar-o"></i><span>News & Events</span></h3>
                </div>
                <ul class="list">
                  <li><a href="#">सोह्रौं वार्षिक साधारण सभा</a>
                    <p>पोखरा,  ४ मंसिर २०७३| जय मनकामना बचत तथा ऋण सहकारी संस्था लि.को संस्थाका अध्यक्ष नारायण पौडेलको अध्यक्षतामा पोखरामा शनिबार सम्पन्न भयो ।</p>
                    <a href="#" class="read-btn">Read More</a> </li>
                  <li><a href="#">सोह्रौं वार्षिक साधारण सभा</a>
                    <p>पोखरा,  ४ मंसिर २०७३| जय मनकामना बचत तथा ऋण सहकारी संस्था लि.को संस्थाका अध्यक्ष नारायण पौडेलको अध्यक्षतामा पोखरामा शनिबार सम्पन्न भयो ।</p>
                    <a href="#" class="read-btn">Read More</a> </li>
                </ul>
              </div>
              <div class="block">
                <div class="section-title-wr">
                  <h3 class="section-title left"><i class="fa fa-bank"></i><span>Deposit Scheme</span></h3>
                </div>
                <ul class="list btn-list">
                  <li><a href="#"> नारी आवधिक <span>बचत </span></a> </li>
                  <li><a href="#">बाल शैक्षिक<span>बचत </span></a> </li>
                  <li><a href="#">मासिक <span>बचत </span></a> </li>
                  <li><a href="#">मुद्दती <span>खाता </span></a> </li>
                </ul>
              </div>
              <div class="block">
                <div class="section-title-wr">
                  <h3 class="section-title left"><i class="fa fa-facebook"></i><span>Follow Us Facebook</span></h3>
                </div>
                
                
              </div>
            </aside>
          </div>
          <div class="col-md-9 right-content">
            <div class="sec-title">
              <h2>सल्लाहकार</h2>
              <div class="decor-line"></div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Pom Lal Giri.jpg" class="img-responsive"> </div>
                  <h2>पदमसिंह गुरूङ</h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Krishna Prasad Dhakal.JPG" class="img-responsive"> </div>
                  <h2>पुष्पेन्द्र पाण्डे</h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Navaraj Tiwari.jpg" class="img-responsive"> </div>
                  <h2>नरेन्द्र ब. भण्डारी</h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Rabindara Raj Paudel.jpg" class="img-responsive"> </div>
                  <h2>चिरञ्जिवी पाण्डे</h2>
                </div>
              </div>
            </div>
            <hr/>
            <div class="sec-title">
              <h2>लेखा समिति</h2>
              <div class="decor-line"></div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Pom Lal Giri.jpg" class="img-responsive"> </div>
                  <h2>सुरेन्द्रमानसिंह भण्डारी <small>(संयोजक)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Krishna Prasad Dhakal.JPG" class="img-responsive"> </div>
                  <h2>सुशिलकुमार श्रेष्ठ <small>(सदस्य)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Navaraj Tiwari.jpg" class="img-responsive"> </div>
                  <h2>स्वागतसिंह भण्डारी<small>(सदस्य)</small></h2>
                </div>
              </div>
              
            </div>
            <hr/>
            <div class="sec-title">
              <h2>ऋण उप–समिति</h2>
              <div class="decor-line"></div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Pom Lal Giri.jpg" class="img-responsive"> </div>
                  <h2>झलक ब. केसी<small>(संयोजक)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Krishna Prasad Dhakal.JPG" class="img-responsive"> </div>
                  <h2>मिलन के.सी.<small>(सदस्य)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Navaraj Tiwari.jpg" class="img-responsive"> </div>
                  <h2>पूर्ण ब. बानिया<small>(सदस्य)</small></h2>
                </div>
              </div>
              
            </div>
            <hr/>
            <div class="sec-title">
              <h2>शिक्षा उप–समिति</h2>
              <div class="decor-line"></div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Pom Lal Giri.jpg" class="img-responsive"> </div>
                  <h2>निरा श्रेष्ठ<small>(संयोजक)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Krishna Prasad Dhakal.JPG" class="img-responsive"> </div>
                  <h2>सबिना पाण्डे क्षेत्री<small>(सदस्य)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Navaraj Tiwari.jpg" class="img-responsive"> </div>
                  <h2>चन्द्रा थापा<small>(सदस्य)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Navaraj Tiwari.jpg" class="img-responsive"> </div>
                  <h2>वेद कुमारी भण्डारी<small>(सदस्य)</small></h2>
                </div>
              </div>
              
            </div>
            <hr/>
            <div class="sec-title">
              <h2>लघुवित्त कार्यक्रम</h2>
              <div class="decor-line"></div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Pom Lal Giri.jpg" class="img-responsive"> </div>
                  <h2>शारदा बस्नेत भण्डारी<small>(संयोजक)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Krishna Prasad Dhakal.JPG" class="img-responsive"> </div>
                  <h2>कमला बस्नेत<small>(सदस्य)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Navaraj Tiwari.jpg" class="img-responsive"> </div>
                  <h2>भगवती बानिया<small>(सदस्य)</small></h2>
                </div>
              </div>
              
            </div>
            <hr/>
            <div class="sec-title">
              <h2>अनुगमन तथा मूल्यङ्कन उप–समिति</h2>
              <div class="decor-line"></div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Pom Lal Giri.jpg" class="img-responsive"> </div>
                  <h2>कृष्ण प्र. अधिकारी<small>(संयोजक)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Krishna Prasad Dhakal.JPG" class="img-responsive"> </div>
                  <h2>राजु के.सी.<small>(सदस्य)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Navaraj Tiwari.jpg" class="img-responsive"> </div>
                  <h2>भरत ब.खत्री<small>(सदस्य)</small></h2>
                </div>
              </div>
              
            </div>
            <hr/>
            <div class="sec-title">
              <h2>सेवाकेन्द्र तथा व्यवस्थापन उप–समिति</h2>
              <div class="decor-line"></div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Pom Lal Giri.jpg" class="img-responsive"> </div>
                  <h2>संजय कुमार अधिकारी<small>(संयोजक)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Krishna Prasad Dhakal.JPG" class="img-responsive"> </div>
                  <h2>झलक भण्डारी मिलन<small>(सदस्य)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Navaraj Tiwari.jpg" class="img-responsive"> </div>
                  <h2>पूर्ण ब. थापा<small>(सदस्य)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Navaraj Tiwari.jpg" class="img-responsive"> </div>
                  <h2>उमेश चन्द्र देवकोटा<small>(सदस्य)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Navaraj Tiwari.jpg" class="img-responsive"> </div>
                  <h2>विष्णु ब. अधिकारी क्षेत्री<small>(सदस्य)</small></h2>
                </div>
              </div>
              <div class="col-md-3">
                <div class="wp-block inverse">
                  <div class="figure"> <img alt="" src="images/Navaraj Tiwari.jpg" class="img-responsive"> </div>
                  <h2>सुदिप अधिकारी<small>(सदस्य)</small></h2>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("footer.php")?>