<?php include("header.php")?>
  
  <!-- MAIN CONTENT -->
  <div class="pg-opt">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Deposit & Interest</h2>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="index.php">Home</a></li>
                        
                        <li class="active">Deposit & Interest</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
  <section class="slice white inset-shadow-1 bb animate-hover-slide">
    <div class="wp-section">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <?php include('sidebar.php')?>
          </div>
          <div class="col-md-9">
            <div class="section-title-wr">
                    <h3 class="section-title left">
                        <span>Deposit Scheme and Interest Rates</span>
                        <small>Li Europan lingues es membres del sam familie. Lor separat existentie es un myth.</small>
                    </h3>
                </div>
                <ul class="list bullet-list">
                	<li>Arun Saving<span class="pull-right">6.50 %</span></li>
					<li>Khutruke Saving <span class="pull-right">6.50 %</span></li>
                    <li>Special Saving<span class="pull-right">8.50 %</span></li>
                    <li>Uddhyami Saving<span class="pull-right">7.50 %</span></li>
					<li>Remittance Saving<span class="pull-right">8.00 %</span></li>
                    <li>Karmachari Saving<span class="pull-right">8.25 %</span></li>
                    <li>Didi bahini Saving<span class="pull-right">8.50 %</span></li>
                    <li>Institutional Account<span class="pull-right">6.50 %</span></li>
                    <li>Special Saving<span class="pull-right">8.50 %</span></li>
                </ul>
                <hr/>
              <div class="section-title-wr">
                    <h3 class="section-title left">
                        <span>Fixed Deposit</span>
                        <small>Li Europan lingues es membres del sam familie. Lor separat existentie es un myth.</small>
                    </h3>
                </div>
                <ul class="list bullet-list">
                	<li>3 Months FD <span class="pull-right">10 %</span></li>
					<li>3 Months FD <span class="pull-right">10.50 %</span></li>
                    <li>3 Months FD <span class="pull-right">11 %</span></li>
                    <li>1 – 2 Years FD <span class="pull-right">13 %</span></li>
					<li>Dirghayu Bhawa FD (3 – 24 Months)<span class="pull-right">10- 13 %</span></li>
                    
                </ul>
                
           
            
            
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include("footer.php")?>